from aws_mte import init_client


def run(k, v):
    print('Initializing Connection')
    s3client = init_client('s3')

    print('Getting Response Object')
    response = s3client.list_buckets()

    print('Adding Tag')
    try:
        for a in response['Buckets']:
            result = s3client.put_bucket_tagging(
                Bucket = '{}'.format(a['Name']),
                Tagging = {
                    'TagSet': [{
                        'Key': '{}'.format(k),
                        'Value': '{}'.format(v)
                        }]})
            print(result)
    except:
        print('Failed')


def dryrun(k, v):
    print('Initializing Connection')
    print('Getting Response Object')
    print('Adding Tag')
    print('There is no dryrun for this, so trust me on this one :)')

__author__ = "Jordan Gregory"
__maintainer__ = "Jordan Gregory"
__email__ = "gregory.jordan.m@gmail.com"
__version__ = "1.0.1"
__license__ = "MIT"
__date__ = "2018 May 26"
__status__ = "Production"
__credits__ = [ "Jordan Gregory", "Michael Pigott", ]
__copyright__ = "Copyright 2018, Gregory Development"

