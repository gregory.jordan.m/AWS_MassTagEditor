from aws_mte import init_paginator, init_client
import botocore.exceptions


def run(k, v):
    print('Initializing Connection')
    ec2client = init_client('ec2')

    print('Getting Response Object')
    ec2page = init_paginator('ec2', 'describe_volumes')

    response = ec2page.paginate()

    print('Adding Tag')
    try:
        for a in response:
            for b in range(0, len(a['Volumes'])):
                result = ec2client.create_tags(
                    Resources = [
                        '{}'.format(a['Volumes'][b]['VolumeId'])
                        ],
                    Tags = [{
                        'Key': '{}'.format(k),
                        'Value': '{}'.format(v)
                        }])
                print(result)
    except:
        print('Failed')


def dryrun(k, v):
    print('Initializing Connection')
    ec2client = init_client('ec2')

    print('Getting Response Object')
    ec2page = init_paginator('ec2', 'describe_volumes')

    response = ec2page.paginate()

    print('Adding Tag')
    try:
        for a in response:
            for b in range(0, len(a['Volumes'])):
                result = ec2client.create_tags(
                    DryRun = True,
                    Resources = [
                        '{}'.format(a['Volumes'][b]['VolumeId'])
                        ],
                    Tags = [{
                        'Key': '{}'.format(k),
                        'Value': '{}'.format(v)
                        }])
                print(result)
    except botocore.exceptions.ClientError as err:
        print(err)
    except:
        print('Failed')


__author__ = "Jordan Gregory"
__maintainer__ = "Jordan Gregory"
__email__ = "gregory.jordan.m@gmail.com"
__version__ = "1.0.1"
__license__ = "MIT"
__date__ = "2018 May 26"
__status__ = "Production"
__credits__ = [ "Jordan Gregory", "Michael Pigott", ]
__copyright__ = "Copyright 2018, Gregory Development"
