from aws_mte import init_client
import botocore.exceptions


def run(k, v):
    print('Initializing Connection')
    ec2client = init_client('ec2')

    print('Getting Response Object')
    response = ec2client.describe_subnets()

    print('Adding Tag')
    try:
        for a in response['Subnets']:
            result = ec2client.create_tags(
                Resources = [
                    '{}'.format(a['SubnetId'])
                    ],
                Tags = [{
                    'Key': '{}'.format(k),
                    'Value': '{}'.format(v)
                    }])
            print(result)
    except:
        print('Failed')


def dryrun(k, v):
    print('Initializing Connection')
    ec2client = init_client('ec2')

    print('Getting Response Object')
    response = ec2client.describe_subnets()

    print('Adding Tag')
    try:
        for a in response['Subnets']:
            result = ec2client.create_tags(
                DryRun = True,
                Resources = [
                    '{}'.format(a['SubnetId'])
                    ],
                Tags = [{
                    'Key': '{}'.format(k),
                    'Value': '{}'.format(v)
                    }])
            print(result)

    except botocore.exceptions.ClientError as err:
        print(err)
    except:
        print('Failed')


__author__ = "Jordan Gregory"
__maintainer__ = "Jordan Gregory"
__email__ = "gregory.jordan.m@gmail.com"
__version__ = "1.0.1"
__license__ = "MIT"
__date__ = "2018 May 26"
__status__ = "Production"
__credits__ = [ "Jordan Gregory", "Michael Pigott", ]
__copyright__ = "Copyright 2018, Gregory Development"
